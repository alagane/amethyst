# Integration with desktop environment

!!! note
    The instructions below concerns Kubuntu 20.04 (the system I use), but I'm aware of feedback concerning other systems.

## Enable compositor

Your window manager compositor needs to be enable to allow transparent background to work.

On Kubuntu 20.04, it's enabled by default, but if you doubt you can check:
*System Settings* > *Hardware* > *Display and Monitor* > *Compositor*, then check if *Enable compositor à startup* is checked.

# Turn off fade effects

Amethyst starts so fast than the fade-in and fade-out windows effects added by you compositor may a significant
incidence on the startup time.

On Kubuntu 20.04, it's set to 200ms by default and you can reduce this time (and set it to 0) by editing a configuration file:

```
sudo nano /usr/share/kwin/effects/kwin4_effect_fade/metadata.desktop
```

Then restart your workspace manager.

## Bind Amethyst to a keyboard shortcut

Amethyst is better if you can start it at anytime with a keyboard shortcut.

On Kubuntu 20.04, it's *System Settings* > *Workspaces* > *Shorcuts* > *Custom shortcut*, then *Edit* button,
*New* > *Global keyboard shortcut* > *Command/URL*, then chose a name, on the *Trigger* tab select shortcut, on the
*Action* tag look for the Amethyst executable, then validate and you are done.
