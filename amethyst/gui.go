// gui.go contains code related to the Graphical User Interface.

package main

import (
	"fmt"
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"github.com/faiface/pixel/imdraw"
	"github.com/faiface/pixel/text"
	"github.com/llgcode/draw2d"
	"github.com/llgcode/draw2d/draw2dimg"
	"golang.org/x/image/colornames"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font/gofont/goregular"
	"image"
	"image/color"
	"log"
	"math"
	"os"
)

const (
	winWidth, winHeight      float64 = 400, 400
	innerRadius, outerRadius float64 = 50, 150
	gap                      float64 = 10
	iconTextSize             float64 = 48
	labelTextSize            float64 = 24
	winCenterX, winCenterY   float64 = winWidth / 2, winHeight / 2
)

// UserInput defines the actions that can be triggered by the user.
type UserInput string

const (
	uiUp    UserInput = "u"
	uiRight UserInput = "r"
	uiDown  UserInput = "d"
	uiLeft  UserInput = "l"
	uiClose UserInput = "c"
	uiNone  UserInput = ""
)

func loadAtlas(fontSize float64) *text.Atlas {
	font, err := truetype.Parse(goregular.TTF)
	if err != nil {
		panic(err)
	}

	face := truetype.NewFace(font, &truetype.Options{Size: fontSize, GlyphCacheEntries: 1})
	return text.NewAtlas(face, text.ASCII)
}

func gui(instructionChan chan Instruction, userInputChan chan UserInput) {
	pxWindow := createWindow()

	log.Println("Entering GUI loop")
	for !pxWindow.Closed() {
		userInput := getUserInput(pxWindow)
		if userInput != uiNone {
			if userInput == uiClose {
				os.Exit(0)
			}
			userInputChan <- userInput
		}

		select {
		case instruction := <-instructionChan:
			drawOnWindow(pxWindow, instruction)
		default:
		}
		pxWindow.Update()
	}
	log.Println("Quitted GUI loop")
	userInputChan <- uiClose
}

func createWindow() *pixelgl.Window {
	log.Println("Creating window")
	var monitorWidth, monitorHeight float64 = pixelgl.PrimaryMonitor().Size()

	config := pixelgl.WindowConfig{
		Title:                  "Amethyst",
		Bounds:                 pixel.R(0, 0, winWidth, winHeight),
		VSync:                  true,
		Resizable:              false,
		Undecorated:            true,
		TransparentFramebuffer: true,
	}

	win, err := pixelgl.NewWindow(config)
	failOnError(err, "Can not create window")

	win.SetPos(pixel.V(monitorWidth/2-winWidth/2, monitorHeight/2-winHeight/2))
	return win
}

func getUserInput(win *pixelgl.Window) UserInput {
	key := uiNone
	if win.JustPressed(pixelgl.KeyLeft) {
		key = uiLeft
	} else if win.JustPressed(pixelgl.KeyRight) {
		key = uiRight
	} else if win.JustPressed(pixelgl.KeyUp) {
		key = uiUp
	} else if win.JustPressed(pixelgl.KeyDown) {
		key = uiDown
	} else if win.JustPressed(pixelgl.KeyEscape) {
		key = uiClose
	}
	if key != "" {
		log.Println("Pressed key " + key)
	}
	return key
}

func drawOnWindow(pxWindow *pixelgl.Window, instruction Instruction) {
	log.Print("Updating window content")

	pxWindow.Clear(color.RGBA{0x00, 0x00, 0x00, 0x00})
	createGemSprite(instruction).Draw(pxWindow, pixel.IM.Moved(pxWindow.Bounds().Center()))
	drawTextIcons(pxWindow, instruction)
	drawLabels(pxWindow, instruction)
}

func drawTextIcons(pxWindow *pixelgl.Window, instruction Instruction) {
	var fontAtlas *text.Atlas = loadAtlas(iconTextSize)
	const iconsRadius float64 = innerRadius + (outerRadius-innerRadius)/2

	textContents := make(map[FacetPosition]string)
	for _, facet := range instruction.Facets {
		textContents[facet.Position] = shorten(facet.Label)
	}

	drawTexts(pxWindow, textContents, iconsRadius, fontAtlas, false, colornames.White, color.RGBA{0, 0, 0, 0})
}

func drawLabels(pxWindow *pixelgl.Window, instruction Instruction) {
	var fontAtlas *text.Atlas = loadAtlas(labelTextSize)
	const labelsRadius float64 = outerRadius + 30

	textContents := make(map[FacetPosition]string)
	for _, facet := range instruction.Facets {
		textContents[facet.Position] = facet.Label
	}

	drawTexts(pxWindow, textContents, labelsRadius, fontAtlas, true, colornames.Blue, colornames.Lightgray)
}

func drawTexts(pxWindow *pixelgl.Window, textContents map[FacetPosition]string, radius float64, atlas *text.Atlas, onEdge bool, color, backgroundColor color.Color) {
	log.Printf("Drawing text on each facets at radius %.2f:\n", radius)

	var anchor pixel.Anchor

	for facetPos, textContent := range textContents {
		if textContent != "" {
			textXPos := winCenterX + radius*math.Cos(RadialPositions[facetPos])
			textYPos := winCenterY + radius*math.Sin(RadialPositions[facetPos])
			if onEdge {
				anchor = anchorPositions[facetPos]
			} else {
				anchor = pixel.Center
			}
			fmt.Printf("  • '%s' at position [%.2f; %.2f] aligned on %s.\n", textContent, textXPos, textYPos, anchor.String())
			pxText := text.New(pixel.V(textXPos, textYPos), atlas)
			pxText.Color = color
			fmt.Fprint(pxText, textContent)

			rect := pxText.Bounds().Moved(pixel.V(0, -2)).AlignedTo(anchor)
			drawRect(pxWindow, rect, backgroundColor)

			pxText.AlignedTo(anchor).Draw(pxWindow, pixel.IM)
		}
	}
}

func drawRect(pxWindow *pixelgl.Window, rect pixel.Rect, color color.Color) {
	imd := imdraw.New(nil)
	imd.Color = color
	imd.Push(rect.Min, rect.Max)
	imd.Rectangle(0)
	imd.Draw(pxWindow)
}

func createGemSprite(instruction Instruction) *pixel.Sprite {
	facetsPositions := facetsPositionbyMode[instruction.GemAttrs.Mode]

	img := image.NewRGBA(image.Rect(0, 0, int(winWidth), int(winHeight)))
	gc := draw2dimg.NewGraphicContext(img)
	gc.BeginPath()
	gc.SetFillColor(colornames.Grey)

	outerGapAngle := math.Asin(gap / 2 / outerRadius)
	innerGapAngle := math.Asin(gap / 2 / innerRadius)
	angle := 2 * math.Pi / float64(len(facetsPositions))

	log.Println("Drawing gem: ", instruction.Facets)
	for i, facetPos := range facetsPositions {
		facet := getFacetByPos(instruction.Facets, facetPos)
		if facet != nil {
			startAngle := float64(i)*angle + math.Pi/2

			path := new(draw2d.Path)
			path.ArcTo(winCenterX, winCenterY, outerRadius, outerRadius, startAngle-angle/2+outerGapAngle, angle-2*outerGapAngle)
			path.ArcTo(winCenterX, winCenterY, innerRadius, innerRadius, startAngle+angle/2-innerGapAngle, -angle+2*innerGapAngle)
			path.Close()
			gc.Fill(path)
		}
	}

	picture := pixel.PictureDataFromImage(img)
	return pixel.NewSprite(picture, picture.Bounds())
}
