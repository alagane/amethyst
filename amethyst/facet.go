// facet.go contains code related to the Facet struct.

package main

import (
	"math"
	"github.com/faiface/pixel"
)

// Facet represents a gem facet, that can be chosed by the user.
type Facet struct {
	Position FacetPosition `json:"pos"`
	Label    string        `json:"label"`
}

// FacetPosition enumerates the positions of a facet in the gem (north, south, etc.).
type FacetPosition string

const (
	fpNorth     FacetPosition = "north"
	fpNorthEast FacetPosition = "north-east"
	fpEast      FacetPosition = "east"
	fpSouthEast FacetPosition = "sout-east"
	fpSouth     FacetPosition = "south"
	fpSouthWest FacetPosition = "south-west"
	fpWest      FacetPosition = "west"
	fpNorthWest FacetPosition = "north-west"
)

// RadialPositions defines the radial position (in radians) of each facet position.
var RadialPositions map[FacetPosition]float64 = map[FacetPosition]float64{
	fpEast:      0.0,
	fpNorthEast: math.Pi / 4 * 1,
	fpNorth:     math.Pi / 4 * 2,
	fpNorthWest: math.Pi / 4 * 3,
	fpWest:      math.Pi / 4 * 4,
	fpSouthWest: math.Pi / 4 * 5,
	fpSouth:     math.Pi / 4 * 6,
	fpSouthEast: math.Pi / 4 * 7,
}

var anchorPositions map[FacetPosition]pixel.Anchor = map[FacetPosition]pixel.Anchor{
	fpEast:      pixel.Right,
	fpSouthEast: pixel.BottomRight,
	fpSouth:     pixel.Bottom,
	fpSouthWest: pixel.BottomLeft,
	fpWest:      pixel.Left,
	fpNorthWest: pixel.TopLeft,
	fpNorth:     pixel.Top,
	fpNorthEast: pixel.TopRight,
}

// GemMode enumerates the gem modes (Duo, Quadro, Octo)
type GemMode []FacetPosition

var (
	duoGemPos    GemMode = []FacetPosition{fpEast, fpWest}
	quadroGemPos GemMode = []FacetPosition{fpNorth, fpEast, fpSouth, fpWest}
	octoGemPos   GemMode = []FacetPosition{fpNorth, fpNorthEast, fpEast, fpSouthEast,
		fpSouth, fpSouthWest, fpWest, fpNorthWest}
	facetsPositionbyMode map[IGMode]GemMode = map[IGMode]GemMode{igmDuo: duoGemPos, igmQuadro: quadroGemPos, igmOcto: octoGemPos}
)

// func (gemMode *GemMode) contains(facetPos FacetPosition) bool {
// 	for _, otherFacetPos := range gemMode {
// 		if facetPos == otherFacetPos {
// 			return true
// 		}
// 	}
// 	return false
// }

func getFacetByPos(facets []Facet, facetPos FacetPosition) *Facet {
	for _, otherFacet := range facets {
		if facetPos == otherFacet.Position {
			return &otherFacet
		}
	}
	return nil
}

// NewFacet creates a new facet at a given position, with "∅" as label.
func NewFacet(position FacetPosition) Facet {
	return Facet{position, "∅"}
}

// ToString returns the string reprensentation of a facet.
func (facet *Facet) ToString() string {
	if facet == nil {
		return "∅"
	}
	return string(facet.Position) + ": " + facet.Label
}
