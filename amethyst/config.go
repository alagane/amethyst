// config.go contains code related to the Amethyst configuration.

package main

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
)

// AppConfig defines the Amethyst configuration.
type AppConfig struct {
	WindowSize  int `yaml:"window_size"`
	OuterSize   int `yaml:"outer_size"`
	InnerSize   int `yaml:"inner_size"`
	KeyDelay    int `yaml:"key_delay"`
	ColorScheme int `yaml:"color_scheme"`
}

// GetConfig returns the user configuration, eventually creating the config file if necessary.
func GetConfig() AppConfig {
	var config AppConfig

	userConfigPath := GetUserResourcePath("config.yml")
	log.Printf("Loading config file from %v...\n", userConfigPath)

	source, err3 := ioutil.ReadFile(userConfigPath)
	if err3 != nil {
		panic(err3)
	}

	err4 := yaml.Unmarshal(source, &config)
	if err4 != nil {
		panic(err4)
	}

	return config
}
