#!/usr/bin/env bash

LOG_FILE="./logger"

echo "[child] Start" >> $LOG_FILE
echo "n"

while read input; do
	echo "[child] Received from parent: $input" >> $LOG_FILE
	if [ "$input" == "u" ]; then
		response="n"
	elif [ "$input" == "r" ]; then
		response="e"
	elif [ "$input" == "d" ]; then
		response="s"
	elif [ "$input" == "l" ]; then
		response="w"
	else
		response="q"
	fi
	echo "[child] Sending to parent: $response" >> $LOG_FILE
	echo "$response"
done

echo "[child] End" >> $LOG_FILE
