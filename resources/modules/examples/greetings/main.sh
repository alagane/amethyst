#!/usr/bin/env bash

if [ $# -eq 0 ]; then
  echo "Garnet"
  echo "Amethyst"
  echo "Pearl"
  echo "Steven"
else
  echo "Greetings $1."
fi
