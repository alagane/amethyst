#!/usr/bin/env bash
# dependency: libnotify (ie. sudo apt install libnotify-bin)

json_path="`dirname "$0"`/gem.json"
json_oneliner=`cat "$json_path" | tr '\n' ' ' | sed 's/[[:space:]]\+/ /g'`
echo "$json_oneliner"

while read input; do
	notify-send "You selected the ${input} facet."
done
